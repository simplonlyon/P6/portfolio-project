Réaliser un site portfolio en HTML/CSS qui sera ensuite hébergé sur www.simplonlyon.fr
Utiliser un template trouvé sur internet pour vous inspirer (ne faite pas le design en partant de zéro). 
N'utilisez pas le HTML/CSS de ce template, uniquement le visuel que vous reproduirez avec votre propre code.